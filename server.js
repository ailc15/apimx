var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/alopez2/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;
var urlClientes = "https://api.mlab.com/api/1/databases/alopez2/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlClientes);
var urlregistrar = "https://api.mlab.com/api/1/databases/alopez2/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var registrarMlab = requestjson.createClient(urlregistrar);
var banxicoUrl;
var urlBanxicoRaiz = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/";


app.listen(port);

var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next()
})

var movimientosJSON = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function (req,res){
  res.send("hemos recibido su peticion de POSt - cambiada");
})

app.put('/', function (req,res){
  res.send("hemos recibido su peticion de PUT");
})

app.delete('/', function (req,res){
  res.send("hemos recibido su peticion de Delete");
})

app.get('/Clientes/:idcliente', function (req,res){
  res.send("Aqui tiene al cliente numero "+ req.params.idcliente);
})

app.get('/v1/Movimientos', function (req,res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function (req,res){
  res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id', function (req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id - 1]);
})

app.get('/v2/MovimientosQuery', function (req,res){
  console.log(req.query);
  res.send("Se recibio el Query ");
})

app.post('/v2/Movimientos', function (req, res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de Alta")
})
// es la consulta del clientes
app.get('/Clientes', function(req, res){
  clienteMlab.get('', function(err, resM, body){
    if(err){
      console.log(body)
    }else {
      res.send(body)
    }
  })
})

//esta es la parte del registro en clientes
app.post('/Clientes', function (req, res){
  clienteMlab.post('',req.body, function(err, resM, body){
    res.send(body)
  })
})
//esta es la parte del LOGIN
app.post("/Login", function(req, res){
  res.set("Access-Control-Allow-Headers","Content-Type");
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';
  //var query = 'q={"email":"'+email+'","password":"'+password+'"}'
  var urlMlab = urlMlabRaiz+"/Usuarios?"+query+"&"+ apiKey;
  console.log(query);
  console.log(urlMlab);
  clienteMlabRaiz = requestjson.createClient(urlMlab);

  clienteMlabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){ // login ok, se encontro 1 doc
          res.status(200).send('Usuario logueado');
      } else {
        res.status(404).send('Usuario no encontrado');
      }
    }
  })
})

//esta es la parte del resgistro de usuarios
app.post("/Registrar", function(req, res){
  registrarMlab.post('',req.body, function(err, resM, body){
    res.send(body)
  })
})

//esta es la parte del la consulta a Banxico
app.post("/Banxico", function(req, res){
  res.set("Access-Control-Allow-Headers","Content-Type");
  var serie = req.body.serie;
  var token = req.body.token;

  var urlBanxico = urlBanxicoRaiz+serie+"?token="+token;

  console.log(urlBanxico);
  banxicoUrl = requestjson.createClient(urlBanxico);

  banxicoUrl.get('', function(err, resM, body){
    if(!err){
      res.send(body);
      console.log(body);
    }else {
        console.log(body);
      }
  })
})
