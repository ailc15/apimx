# Imagen Base
FROM node:latest

#Directorio de la APP en el contenedor
WORKDIR /app

# Copiado de archivos
ADD . /app

# Dependencias
RUN npm install

#Puerto que expongo en el contenedor
EXPOSE 3000

# Comando para ejecutar el servicio
CMD ["npm", "start"]
